local options = {
    mouse = "a",
    shiftwidth = 4,
    tabstop = 4,
    smarttab = true,
    expandtab = true,
    ai = true,
    si = true,
    wrap = false,
    lbr = true,
    tw = 500,
    hidden = true,
    number = true,
    relativenumber = true,
    cursorline = true,
    scrolloff = 999,
    swapfile = false,
    cmdheight = 2,
    ruler = true,
    wildmenu = true,
    list = true,
    listchars = "eol:↲,trail:·",
    showbreak = "↪",
    splitright = true,  -- always vsplit to the right
    splitbelow = true,  -- always hsplit below the current window
    termguicolors = true,
    syntax = "ON",
    hlsearch = false,
    guifont = "FiraCode Nerd Font Mono:h12"
}

for k, v in pairs(options) do
    vim.opt[k] = v
end

-- Vim g
vim.g.python3_host_prog = vim.fn.expand('~/.pyenv/versions/nvim/bin/python')
vim.g.neovide_cursor_vfx_mode = "pixiedust"
vim.g.neovide_cursor_vfx_particle_density = 33
vim.g.neovide_cursor_vfx_particle_lifetime = 3.5
