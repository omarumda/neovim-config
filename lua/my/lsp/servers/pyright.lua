return {
    settings = {
        python = {
            analysis = {
                extraPaths = {
                    "/home/omar/.pyenv/versions/knaps3.8/lib/python3.8/site-packages",
                    "/home/omar/projects/knaps"
                }
            }
        }
    }
}
