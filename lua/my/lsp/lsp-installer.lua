local config_ok, _ = pcall(require, "lspconfig")
if not config_ok then
    return
end

local installer_ok, lsp_installer = pcall(require, "nvim-lsp-installer")
if not installer_ok then
    return
end

local enhance_server_opts = function(server, opts)
    local opts_ok, server_opts = pcall(require, string.format('my.lsp.servers.%s', server))
    if not opts_ok then
        return opts
    end
    return vim.tbl_deep_extend("force", server_opts, opts)
end

lsp_installer.on_server_ready(function(server)
    local opts = {
        on_attach = require("my.lsp.lsp-default-options").on_attach,
        capabilities = require("my.lsp.lsp-default-options").capabilities,
    }
    opts = enhance_server_opts(server.name, opts)
    -- Refer to https://github.com/neovim/nvim-lspconfig/blob/master/doc/server_configurations.md
    server:setup(opts)
end)

local setup = function()
    local signs = {
        { name = "DiagnosticSignError", text = "" },
        { name = "DiagnosticSignWarn", text = "" },
        { name = "DiagnosticSignHint", text = "" },
        { name = "DiagnosticSignInfo", text = "" },
    }

    for _, sign in ipairs(signs) do
        vim.fn.sign_define(sign.name, { texthl = sign.name, text = sign.text, numhl = "" })
    end

    local config = {
        -- disable virtual text
        virtual_text = false,
        -- show signs
        signs = {
            active = signs,
        },
        update_in_insert = true,
        underline = true,
        severity_sort = true,
        float = {
            focusable = false,
            style = "minimal",
            border = "rounded",
            source = "always",
            header = "",
            prefix = "",
        },
    }

    vim.diagnostic.config(config)
end
setup()
