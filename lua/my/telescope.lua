local status_ok, telescope = pcall(require, "telescope")
if not status_ok then
    return
end

local actions = require "telescope.actions"

telescope.setup {
    defaults = {
        mappings = {
            i = {
                ["<C-h>"] = actions.which_key
            },
            n = {
                ["<C-h>"] = actions.which_key
            }
        }
    },
    pickers = {
    },
    extensions = {
    }
}

