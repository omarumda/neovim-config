local opts = { noremap = true, silent = true }

-- local term_opts = { silent = true }

-- Shorten function name
local keymap = vim.api.nvim_set_keymap

-- Remap space as leader key
keymap("", "<Space>", "<Nop>", opts)
vim.g.mapleader = " "
vim.g.maplocalleader = " "


-- Normal Mode remaps
-- Better window nav
keymap ("n", "<C-h>", "<C-w>h", opts)
keymap ("n", "<C-j>", "<C-w>j", opts)
keymap ("n", "<C-k>", "<C-w>k", opts)
keymap ("n", "<C-l>", "<C-w>l", opts)
-- Resize with arrows
keymap("n", "<C-Up>", ":resize -2<CR>", opts)
keymap("n", "<C-Down>", ":resize +2<CR>", opts)
keymap("n", "<C-Left>", ":vertical resize -2<CR>", opts)
keymap("n", "<C-Right>", ":vertical resize +2<CR>", opts)
-- Better remaps
keymap("n", "gf", ":edit <cfile><cr>", opts)                -- Opens file under cursor even if it doesn't exist yet
keymap("n", "Y", "y$", opts)                                -- Make Y work like other capitals
-- Sourcing VIM settings
keymap("n", "<leader>sv", ":luafile $MYVIMRC<CR>", opts)
keymap("n", "<leader>ss", ":luafile %<CR>", opts)
--NVIM Tree
keymap("n", "<leader>e", ":NvimTreeToggle<cr>", opts)
keymap("n", "<A-l>", ":BufferLineCycleNext<cr>", opts)
keymap("n", "<A-h>", ":BufferLineCyclePrev<cr>", opts)
keymap("n", "<A-w>", ":Bdelete<cr>", opts)


-- Visual Mode remaps
-- Stay in indent mode
keymap ("v", ">", ">gv", opts)
keymap ("v", "<", "<gv", opts)

-- Visual Block --
-- Move text up and down
keymap("x", "J", ":move '>+1<CR>gv-gv", opts)
keymap("x", "K", ":move '<-2<CR>gv-gv", opts)
keymap("x", "<A-j>", ":move '>+1<CR>gv-gv", opts)
keymap("x", "<A-k>", ":move '<-2<CR>gv-gv", opts)

-- Telescope
keymap("n", "<leader>ff", "<cmd>Telescope find_files<cr>", opts)
keymap("n", "<leader>fg", "<cmd>Telescope live_grep<cr>", opts)
keymap("n", "<leader>fh", "<cmd>Telescope help_tags<cr>", opts)
keymap("n", "<leader>fr", "<cmd>Telescope lsp_references<cr>", opts)
keymap("n", "<leader>fv", "<cmd>Telescope vim_options<cr>", opts)
